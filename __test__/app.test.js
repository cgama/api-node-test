const supertest = require('supertest')
const app = require("./../src/app")

describe(" TEST::App", () => {
    test("O servidor deve responder a raiz", () => {
        return supertest(app).get('/')
            .then((response) => {
                expect(response.status).toBe(200)
            })
    })
})